package com.apro.paraflight.ui.mapbox.customview

import android.content.Context
import android.util.AttributeSet
import androidx.constraintlayout.widget.ConstraintLayout

class MeterView @JvmOverloads constructor(context: Context, attr: AttributeSet) :
  ConstraintLayout(context, attr)